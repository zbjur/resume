Feature: Show resume information

	Scenario: Show resume information details
           Given: App has been opened
            When: "Resume Screen" is visible to the user
            Then: The bio section is visible
            Then: The experience section is visible
            Then: The education section is visible
            Then: The skills section is visible
            Then: The certification section is visible
            Then: The languages section is visible
            Then: The links section is visible