package com.hsbc.resume.features


import androidx.test.rule.ActivityTestRule
import com.hsbc.resume.mobile.resume.ResumeActivity
import com.hsbc.resume.steps.ShowResumeStep
import com.mauriciotogneri.greencoffee.GreenCoffeeConfig
import com.mauriciotogneri.greencoffee.GreenCoffeeTest
import com.mauriciotogneri.greencoffee.ScenarioConfig
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import java.io.IOException
import java.util.*

@RunWith(Parameterized::class)
class StartUpFeatureFeatureTest(scenarioConfig: ScenarioConfig) : GreenCoffeeTest(scenarioConfig) {

    @Rule
    @JvmField
    var activityTestRule = ActivityTestRule(ResumeActivity::class.java)

    @Test
    fun test() {
        start(ShowResumeStep())
    }

    companion object {
        @Parameterized.Parameters
        @Throws(IOException::class)
        @JvmStatic
        fun scenarios(): Iterable<ScenarioConfig> {
            return GreenCoffeeConfig() // folder to place the screenshot if a test fails
                .withFeatureFromAssets("assets/ShowResumeDetails.feature")
                .scenarios(Locale("en", "GB"))
        }
    }
}