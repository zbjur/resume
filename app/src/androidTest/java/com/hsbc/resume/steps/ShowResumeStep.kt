package com.hsbc.resume.steps

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.rule.ActivityTestRule
import com.hsbc.resume.mobile.resume.ResumeActivity
import com.mauriciotogneri.greencoffee.GreenCoffeeSteps
import org.junit.Rule

class ShowResumeStep : GreenCoffeeSteps() {

    companion object {
        const val EXPERIENCE = "Doświadczenie zawodowe"
        const val EDUCATION = "Wykształcenie"
        const val SKILLS = "Umiejętnośći"
        const val CERTIFICATION = "Kursy, szkolenia"
        const val LANGUAGES = "Języki"
        const val LINKS = "Linki"
    }

    @get:Rule
    val resumeActivityTestRule: ActivityTestRule<ResumeActivity> =
        ActivityTestRule(ResumeActivity::class.java, false, false)

    fun scrollToExperienceSection() =
        RecyclerViewActions.scrollTo<RecyclerView.ViewHolder>(
            ViewMatchers.hasDescendant(ViewMatchers.withText(EXPERIENCE))
        )


    private fun scrollToEducationSection(): RecyclerViewActions.PositionableRecyclerViewAction {
        return RecyclerViewActions.scrollTo<RecyclerView.ViewHolder>(
            ViewMatchers.hasDescendant(ViewMatchers.withText(EDUCATION))
        )
    }

    private fun scrollToSkillsSection(): RecyclerViewActions.PositionableRecyclerViewAction {
        return RecyclerViewActions.scrollTo<RecyclerView.ViewHolder>(
            ViewMatchers.hasDescendant(ViewMatchers.withText(SKILLS))
        )
    }

    private fun scrollToCertificationSection(): RecyclerViewActions.PositionableRecyclerViewAction {
        return RecyclerViewActions.scrollTo<RecyclerView.ViewHolder>(
            ViewMatchers.hasDescendant(ViewMatchers.withText(CERTIFICATION))
        )
    }

    private fun scrollToLanguageSection(): RecyclerViewActions.PositionableRecyclerViewAction {
        return RecyclerViewActions.scrollTo<RecyclerView.ViewHolder>(
            ViewMatchers.hasDescendant(ViewMatchers.withText(LANGUAGES))
        )
    }

    private fun scrollToLinksSection(): RecyclerViewActions.PositionableRecyclerViewAction {
        return RecyclerViewActions.scrollTo<RecyclerView.ViewHolder>(
            ViewMatchers.hasDescendant(ViewMatchers.withText(LINKS))
        )
    }
}