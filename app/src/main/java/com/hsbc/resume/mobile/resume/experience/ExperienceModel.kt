package com.hsbc.resume.mobile

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel

data class ExperienceModel(
    val position: String,
    val location: String,
    val companyName: String,
    val startDate: String,
    val endDate: String,
    val task: String

) : ViewModel