package com.hsbc.resume.mobile.resume.category

import android.view.ViewGroup
import com.github.vivchar.rendererrecyclerviewadapter.ViewRenderer
import com.hsbc.resume.R

class CategoryViewRenderer : ViewRenderer<CategoryModel, CategoryViewHolder>(CategoryModel::class.java) {

    override fun bindView(model: CategoryModel, holder: CategoryViewHolder) {
        holder.mName.text = model.name
    }

    override fun createViewHolder(parent: ViewGroup?): CategoryViewHolder {

        return CategoryViewHolder(inflate(R.layout.category_item, parent))
    }
}