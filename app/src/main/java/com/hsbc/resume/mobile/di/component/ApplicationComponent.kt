package com.hsbc.resume.mobile.di.component

import android.app.Application
import com.hsbc.resume.BaseApplication
import com.hsbc.resume.mobile.di.ViewModelModule
import com.hsbc.resume.mobile.di.module.*
import com.hsbc.resume.mobile.resume.ResumeFragment
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ApplicationModule::class,
        RepositoryModule::class,
        UseCaseModule::class,
        ViewModelModule::class,
        RenderViewModule::class,
        MapperModule::class,
        AndroidModule::class]
)

interface ApplicationComponent : AndroidInjector<BaseApplication> {

    fun inject(fragment: ResumeFragment)

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance application: Application): ApplicationComponent
    }
}