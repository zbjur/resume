package com.hsbc.resume.mobile

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel

data class LinksModel(
    val linkType: String,
    val urlAddress: String
) : ViewModel