package com.hsbc.resume.mobile

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel

data class ResumeModel(
    val name: String,
    val lastName: String,
    val city: String,
    val voivodeship: String,
    val email: String,
    val phoneNumber: String,
    val birthDate: String
) : ViewModel