package com.hsbc.resume.mobile.base

import android.view.ViewGroup
import com.github.vivchar.rendererrecyclerviewadapter.ViewRenderer
import com.hsbc.resume.R
import com.hsbc.resume.mobile.CoursesModel
import com.hsbc.resume.mobile.resume.bio.CertificationTrainingsGroupHolder

class CertificationAndTrainingsViewRenderer : ViewRenderer<CoursesModel, CertificationTrainingsGroupHolder>(CoursesModel::class.java) {

    override fun bindView(coursesModel: CoursesModel, certificationTrainingsGroupHolder: CertificationTrainingsGroupHolder) {
        certificationTrainingsGroupHolder.setCertificationAndTrainingsData(coursesModel)
    }

    override fun createViewHolder(parent: ViewGroup?): CertificationTrainingsGroupHolder {
        return CertificationTrainingsGroupHolder(inflate(R.layout.certification_training_item, parent))
    }
}