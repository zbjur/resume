
package com.fernandocejas.sample.core.platform

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel : ViewModel() {

   var failure: MutableLiveData<Throwable> = MutableLiveData()

    private val compositeDisposable = CompositeDisposable()

    fun addDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }
    override fun onCleared() {
        compositeDisposable.clear()
    }

    protected fun handleFailure(failure: Throwable) {
        this.failure.value = failure
    }
}