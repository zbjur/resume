package com.hsbc.resume.mobile.di.module

import com.github.vivchar.rendererrecyclerviewadapter.DefaultCompositeViewModel
import com.github.vivchar.rendererrecyclerviewadapter.binder.CompositeViewBinder
import com.hsbc.resume.R
import com.hsbc.resume.mobile.base.*
import com.hsbc.resume.mobile.resume.category.CategoryViewRenderer
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RenderViewModule {

    @Singleton
    @Provides
    fun createAveragePriceRenderer() = BioViewRenderer()

    @Singleton
    @Provides
    fun createCertificationAndTrainings() = CertificationAndTrainingsViewRenderer()

    @Singleton
    @Provides
    fun createEducationViewRenderer() = EducationViewRenderer()

    @Singleton
    @Provides
    fun createExperienceViewRenderer() = ExperienceViewRenderer()

    @Singleton
    @Provides
    fun createLinksViewRenderer() = LinksViewRenderer()

    @Singleton
    @Provides
    fun createSkillsViewRenderer() = SkillsViewRenderer()

    @Singleton
    @Provides
    fun createLanguageViewRenderer() = LanguageViewRenderer()

    @Singleton
    @Provides
    fun createCategoryViewRenderer() = CategoryViewRenderer()

    @Singleton
    @Provides
    fun createParentItemViewBinder() = CompositeViewBinder(
        R.layout.parent_item_layout,
        R.id.recycler_view,
        DefaultCompositeViewModel::class.java
    )
}



