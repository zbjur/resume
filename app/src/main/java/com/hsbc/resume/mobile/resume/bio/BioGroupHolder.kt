package com.hsbc.resume.mobile.resume.bio

import android.view.View
import android.widget.TextView
import com.github.vivchar.rendererrecyclerviewadapter.ViewHolder
import com.hsbc.resume.R
import com.hsbc.resume.mobile.ResumeModel


class BioGroupHolder(itemView: View) : ViewHolder(itemView) {

    private var nameAndLastName: TextView? = null
    private var address: TextView? = null
    private var emailText: TextView? = null
    private var phoneNumberText: TextView? = null
    private var birthDate: TextView? = null

    init {
        nameAndLastName = itemView.findViewById(R.id.name_and_surname)
        address = itemView.findViewById(R.id.address_text)
        emailText = itemView.findViewById(R.id.email_text)
        phoneNumberText = itemView.findViewById(R.id.phone_number_text)
        birthDate = itemView.findViewById(R.id.birth_date_text)
    }

    fun setResumeData(resumeModel: ResumeModel) {
        nameAndLastName?.text = resumeModel.name.plus(resumeModel.lastName)
        address?.text = resumeModel.city
        emailText?.text = resumeModel.email
        phoneNumberText?.text = resumeModel.phoneNumber
        birthDate?.text = resumeModel.birthDate
    }
}