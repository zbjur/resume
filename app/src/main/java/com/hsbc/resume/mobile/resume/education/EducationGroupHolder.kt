package com.hsbc.resume.mobile.resume.bio

import android.view.View
import android.widget.TextView
import com.github.vivchar.rendererrecyclerviewadapter.ViewHolder
import com.hsbc.resume.R
import com.hsbc.resume.mobile.EducationModel

class EducationGroupHolder(itemView: View) : ViewHolder(itemView) {

    private var schoolName: TextView? = null
    private var levelName: TextView? = null
    private var faculty: TextView? = null
    private var startDate: TextView? = null
    private var endDate: TextView? = null
    private var educationLevel: TextView? = null

    init {
        schoolName = itemView.findViewById(R.id.position_name)
        levelName = itemView.findViewById(R.id.company_name)
        faculty = itemView.findViewById(R.id.company_location)
        startDate = itemView.findViewById(R.id.start_date)
        endDate = itemView.findViewById(R.id.end_date)
        educationLevel = itemView.findViewById(R.id.education_level_title)
    }

    fun setEducationData(educationModel: EducationModel) {

        schoolName?.text = educationModel.schoolName
        faculty?.text = educationModel.faculty
        levelName?.text = educationModel.courseTitle
        startDate?.text = educationModel.startDate
        endDate?.text = educationModel.endDate
        educationLevel?.text = educationModel.educationLevel
    }
}