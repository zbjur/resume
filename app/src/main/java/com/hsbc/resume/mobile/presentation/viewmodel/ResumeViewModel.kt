package com.hsbc.resume.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import com.fernandocejas.sample.core.platform.BaseViewModel
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import com.hsbc.resume.domain.usecase.resume.ResumeUseCase
import com.hsbc.resume.mobile.base.ApplicationScheduler
import com.hsbc.resume.mobile.model.ResumeMapper
import io.reactivex.functions.Consumer
import javax.inject.Inject

class ResumeViewModel @Inject constructor(
    private val scheduler: ApplicationScheduler,
    private val resumeUseCase: ResumeUseCase, private val resumeMapper: ResumeMapper

) : BaseViewModel() {

    var resumeModel: MutableLiveData<MutableList<ViewModel>> = MutableLiveData()

    fun getResume() {
        addDisposable(
            scheduler.schedule(
                resumeUseCase.getResume(),
                Consumer {
                    handleMovieDetails(resumeMapper.mapToViewModelList(it))
                },
                Consumer {
                    handleFailure(it)
                })
        )
    }

    private fun handleMovieDetails(resumeModelList: MutableList<ViewModel>) {
        resumeModel.value = resumeModelList
    }
}