package com.hsbc.resume.mobile.di.module

import com.google.gson.Gson
import com.hsbc.resume.BuildConfig
import com.hsbc.resume.data.repository.resume.ResumeEndpoint
import com.hsbc.resume.mobile.base.AndroidScheduler
import com.hsbc.resume.mobile.base.ApplicationScheduler
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class ApplicationModule {
    companion object {
        const val CONNECTION_TIMEOUT_IN_SECONDS = 30L
        const val SOCKET_READ_TIMEOUT_IN_SECONDS = 30L
    }

    @Provides
    @Singleton
    open fun providesAndroidScheduler(): ApplicationScheduler =
        AndroidScheduler(AndroidSchedulers.mainThread(), Schedulers.io())

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {

        return OkHttpClient.Builder().apply {
            connectTimeout(CONNECTION_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
            if (BuildConfig.DEBUG) {
                addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            } else {
                readTimeout(SOCKET_READ_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
            }
        }.build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl("https://gist.githubusercontent.com/zbjur/9af0e03f5f229ca1256999d549fb6b6c/raw/")
            .build()
    }

    @Provides
    @Singleton
    fun provideResumeEndpoint(retrofit: Retrofit): ResumeEndpoint {
        return retrofit.create(ResumeEndpoint::class.java)
    }
}