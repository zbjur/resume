package com.hsbc.resume.mobile.resume.list

import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.vivchar.rendererrecyclerviewadapter.CompositeViewRenderer
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import com.github.vivchar.rendererrecyclerviewadapter.ViewState
import com.hsbc.resume.R

class RecyclerViewRenderer :
    CompositeViewRenderer<RecyclerViewModel, RecyclerViewHolder>(RecyclerViewModel::class.java) {

    override fun rebindView(model: RecyclerViewModel, holder: RecyclerViewHolder, payloads: List<Any>) {
        holder.getAdapter().enableDiffUtil()
        holder.getAdapter().setItems(model.items)
    }

    override fun bindView(model: RecyclerViewModel, holder: RecyclerViewHolder) {
        holder.getAdapter().disableDiffUtil()
        holder.getAdapter().setItems(model.items)
        holder.getAdapter().notifyDataSetChanged()
    }

    public override fun createCompositeViewHolder(parent: ViewGroup?): RecyclerViewHolder {
        return RecyclerViewHolder(inflate(R.layout.composite_item, parent))
    }

    override fun createViewState(holder: RecyclerViewHolder): ViewState<*>? {
        return RecyclerViewState(holder)
    }

    override fun createViewStateID(model: RecyclerViewModel): Int {
        return model.id
    }

    override fun createAdapter(): RendererRecyclerViewAdapter {
        return NestedAdapter()
    }

    override fun createLayoutManager(): RecyclerView.LayoutManager {
        return LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    }

}
