package com.hsbc.resume.mobile.resume.bio

import android.view.View
import android.widget.TextView
import com.github.vivchar.rendererrecyclerviewadapter.ViewHolder
import com.hsbc.resume.R
import com.hsbc.resume.mobile.LinksModel

class LinksGroupHolder(itemView: View) : ViewHolder(itemView) {

    private var link: TextView? = null
    private var linkType: TextView? = null

    init {
        link = itemView.findViewById(R.id.link_text)
        linkType = itemView.findViewById(R.id.link_type)
    }

    fun setLinksData(linksModel: LinksModel) {
        link?.text = linksModel.urlAddress
        linkType?.text = linksModel.linkType
    }
}