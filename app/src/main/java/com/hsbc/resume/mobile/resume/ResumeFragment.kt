package com.hsbc.resume.mobile.resume

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import com.hsbc.resume.mobile.platform.BaseFragment
import com.hsbc.resume.presentation.viewmodel.ResumeViewModel
import javax.inject.Inject

class ResumeFragment : BaseFragment() {

    lateinit var recycleView: RecyclerView

    lateinit var resumeViewModel: ResumeViewModel

    @Inject
    lateinit var viewRendererFactory: ViewRendererFactory

    lateinit var mRecyclerViewAdapter: RendererRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mRecyclerViewAdapter = RendererRecyclerViewAdapter()
        viewRendererFactory.buildViewRenderer(mRecyclerViewAdapter)
        activity?.let {
            resumeViewModel = ViewModelProviders.of(it, viewModelFactory).get(ResumeViewModel::class.java)
        }
        initObservers()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(com.hsbc.resume.R.layout.parent_item_layout, container, false)
        recycleView = view.findViewById(com.hsbc.resume.R.id.recycler_view) as RecyclerView
        recycleView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        recycleView.adapter = mRecyclerViewAdapter
        resumeViewModel.getResume()
        return view
    }

    private fun initObservers() {
        resumeViewModel.resumeModel.observe(this, Observer {
            populateAdapter(it)
        })

        resumeViewModel.failure.observe(this, Observer {
            showErrorMessage(it)
        })
    }

    private fun populateAdapter(viewModelList: MutableList<ViewModel>) {
        mRecyclerViewAdapter.setItems(viewModelList)
        mRecyclerViewAdapter.notifyDataSetChanged()
    }

    private fun showErrorMessage(t: Throwable) {
        Toast.makeText(activity, t.message, Toast.LENGTH_SHORT).show()
    }
}




