package com.hsbc.resume.mobile.resume.bio

import android.view.View
import android.widget.TextView
import com.github.vivchar.rendererrecyclerviewadapter.ViewHolder
import com.hsbc.resume.R
import com.hsbc.resume.mobile.CoursesModel

class CertificationTrainingsGroupHolder(itemView: View) : ViewHolder(itemView) {

    private var certificationName: TextView? = null

    init {
        certificationName = itemView.findViewById(R.id.certification_name)
    }

    fun setCertificationAndTrainingsData(coursesModel: CoursesModel) {
        certificationName?.text = coursesModel.name
    }
}