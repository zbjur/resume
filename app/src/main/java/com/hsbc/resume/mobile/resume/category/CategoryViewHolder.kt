package com.hsbc.resume.mobile.resume.category

import android.view.View
import android.widget.TextView
import com.github.vivchar.rendererrecyclerviewadapter.ViewHolder
import com.hsbc.resume.R

class CategoryViewHolder(itemView: View) : ViewHolder(itemView) {
    val mName: TextView = itemView.findViewById<View>(R.id.title) as TextView
}
