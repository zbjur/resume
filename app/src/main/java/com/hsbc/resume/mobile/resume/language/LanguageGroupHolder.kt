package com.hsbc.resume.mobile.resume.bio

import android.view.View
import android.widget.TextView
import com.github.vivchar.rendererrecyclerviewadapter.ViewHolder
import com.hsbc.resume.R
import com.hsbc.resume.mobile.LanguagesModel

class LanguageGroupHolder(itemView: View) : ViewHolder(itemView) {

    private var language: TextView? = null
    private var languageLevel: TextView? = null

    init {
        language = itemView.findViewById(R.id.language_text)
        languageLevel = itemView.findViewById(R.id.language_level_text)
    }

    fun setLanguageData(languageModel: LanguagesModel) {
        language?.text = languageModel.language
        languageLevel?.text = languageModel.level
    }
}