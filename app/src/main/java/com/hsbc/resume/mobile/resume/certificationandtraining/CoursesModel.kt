package com.hsbc.resume.mobile

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel

data class CoursesModel(
    val name: String, val organizer: String,
    val date: String
) : ViewModel