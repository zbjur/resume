package com.hsbc.resume.mobile.resume.bio

import android.view.View
import android.widget.TextView
import com.github.vivchar.rendererrecyclerviewadapter.ViewHolder
import com.hsbc.resume.R
import com.hsbc.resume.mobile.SkillsModel

class SkillsGroupHolder(itemView: View) : ViewHolder(itemView) {

    private var skillName: TextView? = null

    init {
        skillName = itemView.findViewById(R.id.skill_name_text)
    }

    fun setSkillData(skillsModel: SkillsModel) {
        skillName?.text = skillsModel.skill
    }
}