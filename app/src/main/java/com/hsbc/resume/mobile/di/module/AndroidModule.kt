package com.hsbc.resume.mobile.di.module

import com.hsbc.resume.mobile.resume.ResumeActivity
import com.hsbc.resume.mobile.resume.ResumeFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AndroidModule {

    @ContributesAndroidInjector
    abstract fun contributeActivityInjector(): ResumeActivity

    @ContributesAndroidInjector
    abstract fun contributeFragmentInjector(): ResumeFragment
}