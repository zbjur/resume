package com.hsbc.resume.mobile.di.module

import com.hsbc.resume.domain.usecase.resume.ResumeUseCase
import com.hsbc.resume.domain.usecase.resume.ResumeUseCaseImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class UseCaseModule {

    @Binds
    @Singleton
    abstract fun bindsResumeUseCase(resumeUseCase: ResumeUseCaseImpl): ResumeUseCase
}