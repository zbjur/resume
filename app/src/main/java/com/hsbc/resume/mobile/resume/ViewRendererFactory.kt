package com.hsbc.resume.mobile.resume

import com.github.vivchar.rendererrecyclerviewadapter.CompositeViewRenderer
import com.github.vivchar.rendererrecyclerviewadapter.DefaultCompositeViewModel
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import com.github.vivchar.rendererrecyclerviewadapter.binder.CompositeViewBinder
import com.hsbc.resume.mobile.base.*
import com.hsbc.resume.mobile.resume.category.CategoryViewRenderer
import com.hsbc.resume.mobile.resume.list.RecyclerViewRenderer
import javax.inject.Inject

class ViewRendererFactory @Inject constructor(
    private val bioViewRenderer: BioViewRenderer,
    private val certificationAndTrainingsViewRenderer: CertificationAndTrainingsViewRenderer,
    private val educationViewRenderer: EducationViewRenderer,
    private val experienceViewRenderer: ExperienceViewRenderer,
    private val linksViewRenderer: LinksViewRenderer,
    private val skillsViewRenderer: SkillsViewRenderer,
    private val languageViewRenderer: LanguageViewRenderer,
    private val categoryViewRenderer: CategoryViewRenderer

) {
    fun buildViewRenderer(rendererRecyclerViewAdapter: RendererRecyclerViewAdapter) {
        rendererRecyclerViewAdapter.registerRenderer(bioViewRenderer)
        rendererRecyclerViewAdapter.registerRenderer(
            createListRenderer()
                .registerRenderer(certificationAndTrainingsViewRenderer)
                .registerRenderer(educationViewRenderer)
                .registerRenderer(experienceViewRenderer)
                .registerRenderer(linksViewRenderer)
                .registerRenderer(skillsViewRenderer)
                .registerRenderer(languageViewRenderer)
        )
        rendererRecyclerViewAdapter.registerRenderer(categoryViewRenderer)
    }

    private fun createListRenderer(): CompositeViewRenderer<*, *> {
        return RecyclerViewRenderer()
    }
}