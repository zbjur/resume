package com.hsbc.resume.mobile.base

import android.view.ViewGroup
import com.github.vivchar.rendererrecyclerviewadapter.ViewRenderer
import com.hsbc.resume.R
import com.hsbc.resume.mobile.EducationModel
import com.hsbc.resume.mobile.resume.bio.EducationGroupHolder

class EducationViewRenderer : ViewRenderer<EducationModel, EducationGroupHolder>(EducationModel::class.java) {

    override fun bindView(educationModel: EducationModel, educationGroupHolder: EducationGroupHolder) {
        educationGroupHolder.setEducationData(educationModel)
    }

    override fun createViewHolder(parent: ViewGroup?): EducationGroupHolder {
        return EducationGroupHolder(inflate(R.layout.education_item, parent))
    }
}