package com.hsbc.resume.mobile.di.module

import com.hsbc.resume.mobile.model.ResumeMapper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MapperModule {

    @Singleton
    @Provides
    fun createResumeMapper() = ResumeMapper()
}



