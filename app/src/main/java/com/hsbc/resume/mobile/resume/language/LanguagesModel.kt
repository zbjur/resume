package com.hsbc.resume.mobile

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel

data class LanguagesModel(
    val language: String,
    val level: String
) : ViewModel