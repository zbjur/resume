package com.hsbc.resume.mobile.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.hsbc.resume.presentation.viewmodel.ResumeViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ResumeViewModel::class)
    abstract fun bindsResumeViewModel(resumeViewModel: ResumeViewModel): ViewModel
}