package com.hsbc.resume.mobile

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel

data class EducationModel(
    val schoolName: String,
    val educationLevel: String,
    val courseTitle: String,
    val faculty: String,
    val startDate: String,
    val endDate: String
) : ViewModel