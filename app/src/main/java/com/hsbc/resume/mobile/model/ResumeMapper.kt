package com.hsbc.resume.mobile.model

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import com.hsbc.resume.data.model.*
import com.hsbc.resume.mobile.*
import com.hsbc.resume.mobile.resume.category.CategoryModel
import com.hsbc.resume.mobile.resume.list.RecyclerViewModel
import javax.inject.Inject

class ResumeMapper @Inject constructor() {

    fun mapToViewModelList(resume: Resume): MutableList<ViewModel> {
        val resumeList: MutableList<ViewModel> = mutableListOf()
        return resumeList.apply {
            add(
                ResumeModel(
                    name = resume.name,
                    lastName = resume.lastName,
                    city = resume.city,
                    voivodeship = resume.voivodeship,
                    email = resume.email,
                    phoneNumber = resume.phoneNumber,
                    birthDate = resume.birthDate
                )
            )
            add(CategoryModel("Doświadczenie zawodowe"))
            add(RecyclerViewModel(5, mapToExperienceModelList(resume.experience)))
            add(CategoryModel("Wykształcenie"))
            add(RecyclerViewModel(4, mapToEducationList(resume.education)))
            add(CategoryModel("Umiejętnośći"))
            add(RecyclerViewModel(4, mapToSkillList(resume.skills)))
            add(CategoryModel("Kursy, szkolenia"))
            add(RecyclerViewModel(1, mapToCourseList(resume.cours)))
            add(CategoryModel("Języki"))
            add(RecyclerViewModel(3, mapToLanguageList(resume.languages)))
            add(CategoryModel("Linki"))
            add(RecyclerViewModel(2, mapToLinkList(resume.links)))
        }
    }

    private fun mapToExperienceModelList(experienceList: List<Experience>) = mutableListOf<ViewModel>().apply {
        experienceList.forEach {
            add(mapToExperienceModel(it))
        }
    }

    private fun mapToExperienceModel(experience: Experience): ViewModel {
        return ExperienceModel(
            position = experience.position,
            location = experience.location,
            companyName = experience.companyName,
            startDate = experience.startDate,
            endDate = experience.startDate,
            task = "Test task"
        )
    }

    private fun mapToEducationList(educationList: List<Education>) = mutableListOf<ViewModel>().apply {
        educationList?.forEach {
            add(mapToEducationModel(it))
        }
    }

    private fun mapToEducationModel(education: Education): EducationModel {
        return EducationModel(
            schoolName = education.schoolName,
            educationLevel = education.educationLevel,
            courseTitle = education.courseTitle,
            faculty = education.faculty,
            startDate = education.startDate,
            endDate = education.endDate
        )
    }

    private fun mapToSkillList(educationList: List<Skill>) = mutableListOf<ViewModel>().apply {
        educationList?.forEach {
            add(mapToSkillModel(it))
        }
    }

    private fun mapToSkillModel(skill: Skill): SkillsModel {
        return SkillsModel(
            skill = skill.skill
        )
    }

    private fun mapToLinkList(linkList: List<Link>) = mutableListOf<ViewModel>().apply {
        linkList.forEach {
            add(mapToLinkModel(it))
        }
    }

    private fun mapToLinkModel(link: Link): LinksModel {
        return LinksModel(
            linkType = link.linkType,
            urlAddress = link.urlAddress
        )
    }

    private fun mapToCourseList(courseList: List<Course>) = mutableListOf<ViewModel>().apply {
        courseList?.forEach {
            add(mapToCourseModel(it))
        }
    }

    private fun mapToCourseModel(course: Course): CoursesModel {
        return CoursesModel(
            date = course.date,
            name = course.name,
            organizer = course.organizer
        )
    }

    private fun mapToLanguageList(languageList: List<Language>) = mutableListOf<ViewModel>().apply {
        languageList.forEach {
            add(mapToLanguageModel(it))
        }
    }

    private fun mapToLanguageModel(language: Language): LanguagesModel {
        return LanguagesModel(
            language = language.language,
            level = language.level
        )
    }
}