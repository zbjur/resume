package com.hsbc.resume.mobile.base

import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer

open class AndroidScheduler constructor(
    private val observingScheduler: Scheduler,
    private val executingScheduler: Scheduler

) : ApplicationScheduler {

    override fun <C> schedule(
        single: Single<C>,
        onNextAction: Consumer<C>,
        onErrorAction: Consumer<Throwable>
    ): Disposable {
        return single
            .subscribeOn(executingScheduler)
            .observeOn(observingScheduler)
            .subscribe(onNextAction, onErrorAction)
    }
}

