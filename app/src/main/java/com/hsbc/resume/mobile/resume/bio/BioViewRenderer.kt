package com.hsbc.resume.mobile.base

import android.view.ViewGroup
import com.github.vivchar.rendererrecyclerviewadapter.ViewRenderer
import com.hsbc.resume.R
import com.hsbc.resume.mobile.ResumeModel
import com.hsbc.resume.mobile.resume.bio.BioGroupHolder

class BioViewRenderer : ViewRenderer<ResumeModel, BioGroupHolder>(ResumeModel::class.java) {

    override fun bindView(resumeModel: ResumeModel, vehicleModelHolder: BioGroupHolder) {
        vehicleModelHolder.setResumeData(resumeModel)
    }

    override fun createViewHolder(parent: ViewGroup?): BioGroupHolder {
        return BioGroupHolder(inflate(R.layout.bio_item, parent))
    }
}