package com.hsbc.resume.mobile.resume.category

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel

data class CategoryModel(val name: String) : ViewModel

