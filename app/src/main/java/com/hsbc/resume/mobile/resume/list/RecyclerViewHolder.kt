package com.hsbc.resume.mobile.resume.list

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.github.vivchar.rendererrecyclerviewadapter.CompositeViewHolder
import com.hsbc.resume.R

class RecyclerViewHolder(view: View) : CompositeViewHolder(view) {

    init {
        recyclerView = view.findViewById<View>(R.id.recycler_view) as RecyclerView
    }
}