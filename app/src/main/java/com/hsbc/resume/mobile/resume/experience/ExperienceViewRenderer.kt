package com.hsbc.resume.mobile.base

import android.view.ViewGroup
import com.github.vivchar.rendererrecyclerviewadapter.ViewRenderer
import com.hsbc.resume.R
import com.hsbc.resume.mobile.ExperienceModel
import com.hsbc.resume.mobile.resume.bio.ExperienceGroupHolder

class ExperienceViewRenderer : ViewRenderer<ExperienceModel, ExperienceGroupHolder>(ExperienceModel::class.java) {

    override fun bindView(experienceModel: ExperienceModel, experienceGroupHolder: ExperienceGroupHolder) {
        experienceGroupHolder.setExperienceData(experienceModel)
    }

    override fun createViewHolder(parent: ViewGroup?): ExperienceGroupHolder {
        return ExperienceGroupHolder(inflate(R.layout.experience_item, parent))
    }
}