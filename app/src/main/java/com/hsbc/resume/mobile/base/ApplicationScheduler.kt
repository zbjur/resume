package com.hsbc.resume.mobile.base

import io.reactivex.*
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer

interface ApplicationScheduler {

    fun <C> schedule(single: Single<C>, onNextAction: Consumer<C>, onErrorAction: Consumer<Throwable>) : Disposable
}