package com.hsbc.resume.mobile.resume.bio

import android.view.View
import android.widget.TextView
import com.github.vivchar.rendererrecyclerviewadapter.ViewHolder
import com.hsbc.resume.R
import com.hsbc.resume.mobile.ExperienceModel

class ExperienceGroupHolder(itemView: View) : ViewHolder(itemView) {

    private var positionName: TextView? = null
    private var companyName: TextView? = null
    private var companyAddress: TextView? = null
    private var startDate: TextView? = null
    private var endDate: TextView? = null
    private var task: TextView? = null

    init {
        positionName = itemView.findViewById(R.id.position_name)
        companyName = itemView.findViewById(R.id.company_name)
        companyAddress = itemView.findViewById(R.id.company_location)
        startDate = itemView.findViewById(R.id.start_date)
        endDate = itemView.findViewById(R.id.end_date)
        task = itemView.findViewById(R.id.task)
    }

    fun setExperienceData(experienceModel: ExperienceModel) {
        positionName?.text = experienceModel.companyName
        companyName?.text = experienceModel.position
        companyAddress?.text = experienceModel.location
        startDate?.text = experienceModel.startDate
        endDate?.text = experienceModel.endDate
        task?.text = experienceModel.task
    }
}