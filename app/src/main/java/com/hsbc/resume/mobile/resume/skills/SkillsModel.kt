package com.hsbc.resume.mobile

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel

data class SkillsModel(
    val skill: String
) : ViewModel