package com.hsbc.resume.mobile.resume

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.hsbc.resume.R
import dagger.android.AndroidInjection

class ResumeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        setContentView(R.layout.activity_main)
    }
}