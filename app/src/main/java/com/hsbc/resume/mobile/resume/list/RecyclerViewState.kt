package com.hsbc.resume.mobile.resume.list

import com.github.vivchar.rendererrecyclerviewadapter.CompositeViewHolder
import com.github.vivchar.rendererrecyclerviewadapter.CompositeViewState

import java.io.Serializable

class RecyclerViewState(holder: CompositeViewHolder) : CompositeViewState<RecyclerViewHolder>(holder), Serializable {

    override fun restore(holder: RecyclerViewHolder) {
        super.restore(holder)
    }
}
