package com.hsbc.resume.mobile.base

import android.view.ViewGroup
import com.github.vivchar.rendererrecyclerviewadapter.ViewRenderer
import com.hsbc.resume.R
import com.hsbc.resume.mobile.LanguagesModel
import com.hsbc.resume.mobile.resume.bio.LanguageGroupHolder

class LanguageViewRenderer : ViewRenderer<LanguagesModel, LanguageGroupHolder>(LanguagesModel::class.java) {

    override fun bindView(languagesModel: LanguagesModel, languageGroupHolder: LanguageGroupHolder) {
        languageGroupHolder.setLanguageData(languagesModel)
    }

    override fun createViewHolder(parent: ViewGroup?): LanguageGroupHolder {
        return LanguageGroupHolder(inflate(R.layout.language_item, parent))
    }
}