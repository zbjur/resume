package com.hsbc.resume.mobile.base

import android.view.ViewGroup
import com.github.vivchar.rendererrecyclerviewadapter.ViewRenderer
import com.hsbc.resume.R
import com.hsbc.resume.mobile.SkillsModel
import com.hsbc.resume.mobile.resume.bio.SkillsGroupHolder

class SkillsViewRenderer : ViewRenderer<SkillsModel, SkillsGroupHolder>(SkillsModel::class.java) {

    override fun bindView(skillsModel: SkillsModel, vehicleModelHolder: SkillsGroupHolder) {
        vehicleModelHolder.setSkillData(skillsModel)
    }

    override fun createViewHolder(parent: ViewGroup?): SkillsGroupHolder {
        return SkillsGroupHolder(inflate(R.layout.skills_item, parent))
    }
}