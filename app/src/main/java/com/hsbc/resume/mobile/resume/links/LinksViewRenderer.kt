package com.hsbc.resume.mobile.base

import android.view.ViewGroup
import com.github.vivchar.rendererrecyclerviewadapter.ViewRenderer
import com.hsbc.resume.R
import com.hsbc.resume.mobile.LinksModel
import com.hsbc.resume.mobile.resume.bio.LinksGroupHolder

class LinksViewRenderer : ViewRenderer<LinksModel, LinksGroupHolder>(LinksModel::class.java) {

    override fun bindView(linksModel: LinksModel, LinksGroupHolder: LinksGroupHolder) {
        LinksGroupHolder.setLinksData(linksModel)
    }

    override fun createViewHolder(parent: ViewGroup?): LinksGroupHolder {
        return LinksGroupHolder(inflate(R.layout.links_item, parent))
    }
}