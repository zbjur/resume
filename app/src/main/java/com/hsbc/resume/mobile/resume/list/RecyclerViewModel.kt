package com.hsbc.resume.mobile.resume.list

import com.github.vivchar.rendererrecyclerviewadapter.CompositeViewModel
import com.github.vivchar.rendererrecyclerviewadapter.DefaultCompositeViewModel
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel

class RecyclerViewModel(val id: Int, items: MutableList<ViewModel>) : DefaultCompositeViewModel(items) {

    override fun equals(o: Any?): Boolean {
        return o is CompositeViewModel && mItems == o.items
    }

    override fun hashCode(): Int {
        return id
    }

    override fun toString(): String {
        return javaClass.simpleName + "{" + mItems.toString() + "}"
    }
}