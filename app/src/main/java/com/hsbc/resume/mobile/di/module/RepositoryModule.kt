package com.hsbc.resume.mobile.di.module

import com.hsbc.resume.data.repository.resume.ResumeRepositoryImpl
import com.hsbc.resume.domain.repository.ResumeRepositoryApi
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun bindResumeRepositoryApi(resumeRepositoryApi: ResumeRepositoryImpl): ResumeRepositoryApi
}