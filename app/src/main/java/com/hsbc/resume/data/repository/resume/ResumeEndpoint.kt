package com.hsbc.resume.data.repository.resume

import com.hsbc.resume.data.model.Resume
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.POST

interface ResumeEndpoint {

    @GET("625d29c5e08c26128749e7425af77b683a2dc33d/")
    fun getResume(): Single<Resume>
}