package com.hsbc.resume.data.model
import com.google.gson.annotations.SerializedName

data class Language (

	@SerializedName("language") val language : String,
	@SerializedName("level") val level : String
)