package com.hsbc.resume.data.repository.resume

import com.hsbc.resume.domain.repository.ResumeRepositoryApi
import javax.inject.Inject

class ResumeRepositoryImpl @Inject constructor(private val resumeApi: ResumeApi) : ResumeRepositoryApi {
    override fun getResume() = resumeApi.getResume()
}