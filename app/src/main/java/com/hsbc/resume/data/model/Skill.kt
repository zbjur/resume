package com.hsbc.resume.data.model
import com.google.gson.annotations.SerializedName

data class Skill (

	@SerializedName("skill") val skill : String
)