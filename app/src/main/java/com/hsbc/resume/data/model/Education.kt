package com.hsbc.resume.data.model
import com.google.gson.annotations.SerializedName

data class 	Education (

	@SerializedName("schoolName") val schoolName : String,
	@SerializedName("educationLevel") val educationLevel : String,
	@SerializedName("courseTitle") val courseTitle : String,
	@SerializedName("faculty") val faculty : String,
	@SerializedName("startDate") val startDate : String,
	@SerializedName("endDate") val endDate : String
)