package com.hsbc.resume.data.model

data class Resume (

    val name : String,
    val lastName : String,
    val city : String,
    val voivodeship : String,
    val email : String,
    val phoneNumber : String,
    val birthDate : String,
    val experience : List<Experience>,
    val education : List<Education>,
    val skills : List<Skill>,
    val links : List<Link>,
    val cours : List<Course>,
    val languages : List<Language>
)