package com.hsbc.resume.data.model
import com.google.gson.annotations.SerializedName

data class Course (

    @SerializedName("name") val name : String,
    @SerializedName("organizer") val organizer : String,
    @SerializedName("date") val date : String
)