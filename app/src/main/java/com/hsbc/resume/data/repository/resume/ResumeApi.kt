package com.hsbc.resume.data.repository.resume

import javax.inject.Inject

class ResumeApi @Inject constructor(private val resumeApi: ResumeEndpoint) {

    fun getResume() = resumeApi.getResume()
}