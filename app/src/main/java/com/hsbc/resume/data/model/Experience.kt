package com.hsbc.resume.data.model
import com.google.gson.annotations.SerializedName

data class Experience (

	@SerializedName("position") val position : String,
	@SerializedName("location") val location : String,
	@SerializedName("companyName") val companyName : String,
	@SerializedName("startDate") val startDate : String,
	@SerializedName("endDate") val endDate : String,
	@SerializedName("task") val task : String
)