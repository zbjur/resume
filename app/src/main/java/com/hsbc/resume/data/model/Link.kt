package com.hsbc.resume.data.model
import com.google.gson.annotations.SerializedName

data class Link (

	@SerializedName("linkType") val linkType : String,
	@SerializedName("urlAddress") val urlAddress : String
)