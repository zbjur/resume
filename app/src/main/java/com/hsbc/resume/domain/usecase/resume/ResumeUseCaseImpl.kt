package com.hsbc.resume.domain.usecase.resume

import com.hsbc.resume.domain.repository.ResumeRepositoryApi
import javax.inject.Inject

class ResumeUseCaseImpl @Inject constructor(private val resumeRepositoryApi: ResumeRepositoryApi) : ResumeUseCase {
    override fun getResume() = resumeRepositoryApi.getResume()
}