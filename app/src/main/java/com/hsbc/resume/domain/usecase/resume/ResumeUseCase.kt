package com.hsbc.resume.domain.usecase.resume

import com.hsbc.resume.data.model.Resume
import io.reactivex.Single

interface ResumeUseCase {
    fun getResume(): Single<Resume>
}