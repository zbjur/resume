package com.hsbc.resume.domain.repository

import com.hsbc.resume.data.model.Resume
import io.reactivex.Single

interface ResumeRepositoryApi {
    fun getResume() :Single<Resume>
}