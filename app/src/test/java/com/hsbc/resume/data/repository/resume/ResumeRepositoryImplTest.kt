package com.hsbc.resume.data.repository.resume

import com.hsbc.ResumeDataFactory
import com.hsbc.resume.domain.repository.ResumeRepositoryApi
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test

class ResumeRepositoryImplTest {

    private val mockResumeApi: ResumeApi = mock {
        on { getResume() }.thenReturn(Single.just(ResumeDataFactory.makeResumeData()))
    }

    private lateinit var systemUnderTest: ResumeRepositoryApi

    @Before
    fun setUp() {
        systemUnderTest = ResumeRepositoryImpl(mockResumeApi)
    }

    @Test
    fun `load resume`() {
        systemUnderTest.getResume().test()
            .assertNoErrors()
            .assertValueCount(1)
            .assertComplete()

        verify(mockResumeApi).getResume()
    }

    @Test
    fun `load resume returns error on failure`() {
        val error = Throwable()

        whenever(mockResumeApi.getResume()).thenReturn(Single.error(error))

        systemUnderTest.getResume().test().assertError(error)
    }
}