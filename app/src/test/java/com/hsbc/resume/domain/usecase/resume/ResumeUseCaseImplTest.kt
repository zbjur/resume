package com.hsbc.resume.domain.usecase.resume

import com.hsbc.ResumeDataFactory
import com.hsbc.resume.domain.repository.ResumeRepositoryApi
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test

class ResumeUseCaseImplTest {
    private val mockResumeRepositoryApi: ResumeRepositoryApi = mock {
        on { getResume() }.thenReturn(Single.just(ResumeDataFactory.makeResumeData()))
    }

    private lateinit var systemUnderTest: ResumeUseCase

    @Before
    fun setUp() {
        systemUnderTest = ResumeUseCaseImpl(mockResumeRepositoryApi)
    }

    @Test
    fun `load resume`() {
        systemUnderTest.getResume().test()
            .assertNoErrors()
            .assertValueCount(1)
            .assertComplete()

        verify(mockResumeRepositoryApi).getResume()
    }

    @Test
    fun `load resume returns error on failure`() {
        val error = Throwable()

        whenever(mockResumeRepositoryApi.getResume()).thenReturn(Single.error(error))

        systemUnderTest.getResume().test().assertError(error)
    }
}