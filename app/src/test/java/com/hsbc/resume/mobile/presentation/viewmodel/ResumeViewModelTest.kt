package com.hsbc.resume.mobile.presentation.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import com.hsbc.AndroidTestScheduler
import com.hsbc.ResumeDataFactory
import com.hsbc.resume.domain.usecase.resume.ResumeUseCase
import com.hsbc.resume.mobile.ResumeModel
import com.hsbc.resume.mobile.base.ApplicationScheduler
import com.hsbc.resume.mobile.model.ResumeMapper
import com.hsbc.resume.presentation.viewmodel.ResumeViewModel
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ResumeViewModelTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private val scheduler: ApplicationScheduler = spy(AndroidTestScheduler())

    private val mockResumeUseCase: ResumeUseCase = mock {
        on { getResume() }.thenReturn(Single.just(ResumeDataFactory.makeResumeData()))
    }

    private var mockResumeMapper: ResumeMapper = mock {
        on { mapToViewModelList(any()) }.thenReturn(ResumeDataFactory.makeResumeModelList())
    }

    private lateinit var systemUnderTest: ResumeViewModel

    private val observer: Observer<MutableList<ViewModel>> = mock()

    private inline fun <reified T> mock(): T = Mockito.mock(T::class.java)

    @Before
    fun setUp() {
        systemUnderTest = ResumeViewModel(scheduler, mockResumeUseCase, mockResumeMapper)
        systemUnderTest.resumeModel.observeForever(observer)
    }

    @Test
    fun `retrieves resume details`() {

        val expectedName = "John"
        val expectedCity = "Katowice"
        val expectedPhoneNumber = "3434556"

        systemUnderTest.getResume()

        verify(mockResumeUseCase).getResume()
        verify(mockResumeMapper).mapToViewModelList(any())

        systemUnderTest.resumeModel.observeForever {
            val resume = it[0] as ResumeModel
            assertEquals(expectedName, resume.name)
            assertEquals(expectedCity, resume.city)
            assertEquals(expectedPhoneNumber, resume.phoneNumber)
        }
    }

    @Test
    fun `shows error when retrieving resume details`() {
        whenever(mockResumeUseCase.getResume()).thenReturn(Single.error(Throwable("Api error")))

        systemUnderTest.getResume()

        systemUnderTest.failure.observeForever{
            assertEquals("Api error", it.message)  }
    }
}