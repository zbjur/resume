package com.hsbc.resume.mobile.model

import com.hsbc.ResumeDataFactory
import com.hsbc.resume.mobile.ExperienceModel
import com.hsbc.resume.mobile.ResumeModel
import com.hsbc.resume.mobile.resume.category.CategoryModel
import com.hsbc.resume.mobile.resume.list.RecyclerViewModel
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class ResumeMapperTest {

    lateinit var resumeMapper: ResumeMapper

    @Before
    fun setUp() {
        resumeMapper = ResumeMapper()
    }

    @Test
    fun `map to model list`() {
        val expectedName = "John"
        val expectedExperienceCategoryName = "Doświadczenie zawodowe"
        val expectedEducationCategoryName = "Wykształcenie"
        val expectedExperience = "Android Developer"

        val actualValueModel = resumeMapper.mapToViewModelList(ResumeDataFactory.makeResumeData())

        val actualResumeModel = actualValueModel[0] as ResumeModel
        val actualExperienceCategoryModel = actualValueModel[1] as CategoryModel
        val actualRecycleModel = actualValueModel[2] as RecyclerViewModel
        val actualExperience = actualRecycleModel.items[0] as ExperienceModel

        val actualEducationCategoryModel = actualValueModel[3] as CategoryModel

        assertEquals(expectedName, actualResumeModel.name)
        assertEquals(expectedExperienceCategoryName, actualExperienceCategoryModel.name)
        assertEquals(expectedExperience, actualExperience.position)
        assertEquals(expectedEducationCategoryName, actualEducationCategoryModel.name)
    }
}