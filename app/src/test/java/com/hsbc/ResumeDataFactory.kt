package com.hsbc

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import com.hsbc.resume.data.model.*
import com.hsbc.resume.mobile.ResumeModel

object ResumeDataFactory {

    fun makeResumeModelList() = makeResumeModel(makeResumeData())

    private fun makeResumeModel(resume: Resume): MutableList<ViewModel> {
        val resumeList: MutableList<ViewModel> = mutableListOf()
        return resumeList.apply {
            add(ResumeModel(
                    name = resume.name,
                    lastName = resume.lastName,
                    city = resume.city,
                    voivodeship = resume.voivodeship,
                    email = resume.email,
                    phoneNumber = resume.phoneNumber,
                    birthDate = resume.birthDate
                )
            )
        }
    }

    fun makeResumeData() = Resume(
        name = "John",
        lastName = "Malkovich",
        city = "Katowice",
        voivodeship = "Sląskie",
        email = "j.m@gmail.com",
        birthDate = "1953.02.05",
        phoneNumber = "3434556",
        experience = makeExperienceModelList(),
        education = makeEducationList(),
        skills = makeSkillList(),
        links = makeLinkList(),
        cours = makeCourseList(),
        languages = makeLanguageList()
    )

    private fun makeExperienceModelList() = mutableListOf<Experience>().apply {
        add(makeExperienceModel("Katowice"))
        add(makeExperienceModel("Kraków"))
        add(makeExperienceModel("Gdańsk"))
    }

    private fun makeExperienceModel(location: String): Experience {
        return Experience(
            position = "Android Developer",
            location = location,
            companyName = "Software company",
            startDate = "1983.03.04",
            endDate = "1987.03.04",
            task = "Software development"
        )
    }

    private fun makeEducationList() = mutableListOf<Education>().apply {
        add(makeEducationModel("Test school 1"))
        add(makeEducationModel("Test school 2"))
    }

    private fun makeEducationModel(schoolName: String): Education {
        return Education(
            schoolName = schoolName,
            educationLevel = "Magister",
            courseTitle = "Licencjat",
            faculty = "Inżynieria oprogramowania",
            startDate = "1950.10.01",
            endDate = "1955.06.30"
        )
    }

    private fun makeSkillList() = mutableListOf<Skill>().apply {
        add(makeSkill("Uml"))
        add(makeSkill("Java"))
    }

    private fun makeSkill(skill: String): Skill {
        return Skill(
            skill = skill
        )
    }

    private fun makeLinkList() = mutableListOf<Link>().apply {
        add(makeLink("https://siiportal.sii.pl"))
        add(makeLink("https://google.com"))
    }

    private fun makeLink(link: String): Link {
        return Link(
            linkType = "company url",
            urlAddress = link
        )
    }

    private fun makeCourseList() = mutableListOf<Course>().apply {
        add(makeCourse("Course 1"))
        add(makeCourse("Course 2"))
    }

    private fun makeCourse(courseName: String): Course {
        return Course(
            date = "Test",
            name = courseName,
            organizer = "Test"
        )
    }

    private fun makeLanguageList() = mutableListOf<Language>().apply {
        add(makeLanguage("Angielski"))
        add(makeLanguage("Węgierski"))
    }


    private fun makeLanguage(language: String): Language {
        return Language(
            language = language,
            level = "Test level"
        )
    }
}