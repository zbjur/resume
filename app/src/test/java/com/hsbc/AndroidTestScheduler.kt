package com.hsbc


import com.hsbc.resume.mobile.base.AndroidScheduler
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.TestScheduler

class AndroidTestScheduler @JvmOverloads constructor(
    private val observing: Scheduler = Schedulers.trampoline(),
    private val executing: Scheduler = Schedulers.trampoline(),
    private val testScheduler: TestScheduler = TestScheduler()
) : AndroidScheduler(observing, executing)